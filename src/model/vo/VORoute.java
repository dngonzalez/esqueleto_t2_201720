package model.vo;

import java.io.BufferedReader;
import java.io.FileReader;

import model.data_structures.RingList;

/**
 * Representation of a route object
 */
public class VORoute {

	String id;
	String agency;
	String short_name;
	String long_name;
	String desc;
	String type;
	String url;
	String color;
	String text_color;
	
	RingList<VOTrip> trips;

	public VORoute(String pId, String pAgency, String pShort, String pLong, String pDesc, String pType, String pUrl, String pColor, String pText )
	{
		id=pId;
		agency=pAgency;
		short_name=pShort;
		long_name=pLong;
		desc=pDesc;
		type=pType;
		url=pUrl;
		color=pColor;
		text_color=pText;
	}
	
	/**
	 * @return id - Route's id number
	 */
	public String id() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @return name - route name
	 */
	public String shortName() {
		// TODO Auto-generated method stub
		return short_name;
	}
	
	public String longName() {
		// TODO Auto-generated method stub
		return long_name;
	}
	
	public String agency(){
		return agency;
	}
	
	public String desc() {
		// TODO Auto-generated method stub
		return desc;
	}
	
	public String type() {
		// TODO Auto-generated method stub
		return type;
	}
	
	public String url() {
		// TODO Auto-generated method stub
		return url;
	}
	
	public String color() {
		// TODO Auto-generated method stub
		return color;
	}
	
	public String textColor() {
		// TODO Auto-generated method stub
		return text_color;
	}
	
	public void loadTrips(String tripsFile){
		
		trips= new RingList<VOTrip>();
		try {
            BufferedReader bf = new BufferedReader( new FileReader( tripsFile ) );
            String line = bf.readLine( );
            
            String[] parts=line.split(",");
            
            String route_id=parts[0].trim();
            String service_id=parts[1].trim();
            String trip_id=parts[2].trim();
            String trip_Headsing=parts[3].trim();
            String short_name=parts[4].trim();
            String direction=parts[5].trim();
            String block_id=parts[6].trim();
            String shape_id=parts[7].trim();
            String wheelchair=parts[8].trim();
            String bikes=parts[9].trim();
            
            if(id.equals(route_id)){
            
            	VOTrip newTrip= new VOTrip(route_id, service_id, trip_Headsing, short_name, direction, block_id, shape_id, wheelchair, bikes);
            	trips.add(newTrip);
            }
            
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	

}
