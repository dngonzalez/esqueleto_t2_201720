package model.vo;

public class VOTrip 
{
	String route_id,service_id,trip_id,trip_headsign,trip_short_name,direction_id,block_id,shape_id,wheelchair_accessible,bikes_allowed;
	
	public VOTrip(String pRouteId, String pServiceId, String pTripHead, String pShort, String pDirection,String pBLock,String pShape, String pWheelchair, String pBikes )
	{
		route_id=pRouteId;
		service_id=pServiceId;
		trip_headsign=pTripHead;
		trip_short_name=pShort;
		block_id=pBLock;
		shape_id=pShape;
		direction_id=pDirection;
		wheelchair_accessible=pWheelchair;
		bikes_allowed=pBikes;
	}

}
