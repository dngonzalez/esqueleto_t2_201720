package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;

import com.sun.corba.se.impl.orbutil.graph.Node;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;

public class STSManager implements ISTSManager {

	DoubleLinkedList<VORoute> rutas;
	
	public void loadRoutes(String routesFile) {
		// TODO Auto-generated method stub
		
		rutas= new DoubleLinkedList<VORoute>();
		try
		{
            BufferedReader bf = new BufferedReader( new FileReader( routesFile ) );
            String line = bf.readLine( );
            
            while(line!=null)
            {
            	String[] parts= line.split(",");
            	String route_id=parts[0].trim();
            	String agency_id=parts[1].trim();
            	String short_name=parts[2].trim();
            	String long_name=parts[3].trim();
            	String route_desc=parts[4].trim();
            	String route_type=parts[5].trim();
            	String route_url=parts[6].trim();
            	String route_color=parts[7].trim();
            	String text_color=parts[0].trim();
            	
            	VORoute newRoute= new VORoute(route_id, agency_id, short_name, long_name, route_desc, route_type, route_url, route_color, text_color);
            	rutas.add(newRoute);	            	
            }
            
            
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	public void loadTrips(String tripsFile) 
	{
		// TODO Auto-generated method stub
		int indx=0;
		while(rutas.getSize()>indx)
		{
			VORoute actual= rutas.getElement(indx);
			actual.loadTrips(tripsFile);
			indx++;
		}	
	}

	public void loadStopTimes(String stopTimesFile) {
		// TODO Auto-generated method stub
		
	}
	
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		
	}

	public IList<VORoute> routeAtStop(String stopName) {
		// TODO Auto-generated method stub
		return null;
	}

	public IList<VOStop> stopsRoute(String routeName, String direction) {
		// TODO Auto-generated method stub
		return null;
	}

}
