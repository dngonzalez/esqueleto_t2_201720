package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.DoubleLinkedList.Node;


public class RingList<T> implements IList<T>
{
	private Node<T> primera = null;
	private Node<T> ultima = null;
	private long tama�o = 0;
	private Node<T> actualL = primera;
	
	public RingList()
	{	
	}

	//DONE
	public void set(int pos, T pElemento)
	{
		Node<T> actual = primera;
		if(actual == null || pos > tama�o || pos < 0) throw new IndexOutOfBoundsException();
		if(pos == tama�o-1) ultima.siguiente = new Node<T>(pElemento, null, ultima);
		else
		{
			for (int i = 0; i != pos; i++, actual = actual.siguiente);
			actual.elemento = pElemento;
		}
	}

	//DONE
	public void remove(int pos)
	{
		Node<T> actual = primera;
		if(actual == null || pos > tama�o || pos < 0) throw new IndexOutOfBoundsException();
		else if(pos == 0) {
			primera = actual.siguiente; primera.anterior = ultima; 
			ultima.siguiente = primera; tama�o--;
		}
		else if(pos == tama�o-1) {
			ultima = ultima.anterior; ultima.siguiente = primera; 
			primera.anterior = ultima; tama�o--;
		}
		else
		{
			for(int i = 0; i != pos--; i++ , actual = actual.siguiente);
			actual.siguiente = actual.siguiente.siguiente;
			actual.siguiente.anterior = actual; tama�o--;
		}
	}

	//DONE
	public int IndexOf(T elemento)
	{
		int  resp = -1;
		if(elemento == null || primera == null) return resp;
		else
		{
			Node<T> actual = primera;
			for (resp = 0; resp < tama�o; resp++, actual = actual.siguiente) 
				if(actual.elemento == elemento) return resp;
		}
		return resp;
	}

	//DONE
	public boolean isEmpty()
	{
		return primera == null? true: false;
	}

	//DONE
	public boolean contains(T elemento) throws Exception
	{
		if(primera == null) return false;
		else
		{ 
			Node<T> actual = primera;
			for (int i = 0; i < tama�o; i++, actual = actual.siguiente) 
				if(actual.elemento == elemento) return true;
			return false;
		}
	}

	//DONE
	public boolean contains(T[] elementos)
	{
		if(primera == null) return false;
		else
		{
			int correctos = 0;
			Node<T> actual = primera;
			for (int i = 0; i < elementos.length; i++) 
			{
				boolean encontrado = false;
				for (int j = 0; j < tama�o && !encontrado; j++, actual = actual.siguiente)
					if(actual.elemento == elementos[i]) correctos ++; encontrado = true;
			}
			if(correctos == elementos.length) return true;
			return false;
		}
	}
	
	public void add(T object) {
		// TODO Auto-generated method stub
		Node<T> actual = primera;
		primera = new Node<T>(object, actual, ultima);
		if(actual != null){
			actual.anterior = primera; ultima.siguiente = primera; tama�o++;
		}
		else{
			actualL = ultima = ultima.anterior = ultima.siguiente =
					primera.anterior = primera.siguiente = primera; 
			tama�o++;
		}
	}
	
	public void addAtEnd(T object) {
		// TODO Auto-generated method stub
		Node<T> actual = ultima;
		ultima = new Node<T>(object, primera, actual);
		if(actual != null){
			actual.siguiente = ultima; primera.anterior = ultima; tama�o++;
		}
		else{
			actualL = primera = primera.siguiente = primera.anterior =
					ultima.anterior = ultima.siguiente = ultima;
			tama�o++;
		}
	}

	public void addAtK(T object, int K) {
		// TODO Auto-generated method stub
		Node<T> actual = primera;
		if(K > tama�o || K < 0) throw new IndexOutOfBoundsException();
		if(K == 0){
			add(object);
		}
		else if(K == tama�o-1){
			addAtEnd(object);
		}
		else
		{
			for(int i = 0; i != K--; i++ , actual = actual.siguiente);
			Node<T> x = new Node<T>(object, actual.siguiente, actual);
			actual.siguiente.anterior = x; actual.siguiente = x;
			tama�o++;
		}
	}

	public T getElement(int k) {
		// TODO Auto-generated method stub
		Node<T> actual = primera;
		if(actual == null || k > tama�o || k < 0) throw new IndexOutOfBoundsException();
		for (int i = 0; i!=k; i++, actual = actual.siguiente);
		return actual.elemento;
	}

	public T getCurrentElement() {
		// TODO Auto-generated method stub
		return actualL.elemento;
	}

	public void next() {
		// TODO Auto-generated method stub
		actualL = actualL.siguiente;
	}

	public void previous() {
		// TODO Auto-generated method stub
		actualL = actualL.anterior;
	}

	public long getSize() {
		// TODO Auto-generated method stub
		return tama�o;
	}

	public T delete(T object) {
		// TODO Auto-generated method stub
		T eliminado = null;
		boolean eliminar = false;
		Node<T> actual = primera;
		if(actual == null) throw new IndexOutOfBoundsException();
		for(int i = 0; i < tama�o && !eliminar; i++, actual = actual.siguiente){
			if(actual.elemento == object){
				actual.anterior.siguiente = actual.siguiente; 
				actual.siguiente.anterior = actual.anterior; eliminar = true; 
				eliminado = actual.elemento; tama�o--;
				if(actualL.elemento == eliminado){
					if(tama�o == 0) actualL = null;
					else next();
				}
			}
		}
		return eliminado;
	}

	public T deleteAtK(int k) {
		// TODO Auto-generated method stub
		T eliminado = null;
		Node<T> actual = primera;
		if(actual == null || k > tama�o || k < 0) throw new IndexOutOfBoundsException();
		if(k == 0) {
			eliminado = primera.elemento; primera = actual.siguiente; 
			primera.anterior = ultima; tama�o--;
			if(actualL.elemento == eliminado){
				if(tama�o == 0) actualL = null;
				else next();
			}
		}
		else if(k == tama�o-1) {
			eliminado = ultima.elemento; ultima = ultima.anterior;
			ultima.siguiente = primera; tama�o--;
			if(actualL.elemento == eliminado){
				if(tama�o==0) actualL = null;
				else next();
			}
		}
		else
		{
			for(int i = 0; i != k--; i++ , actual = actual.siguiente);
			eliminado = actual.siguiente.elemento;
			actual.siguiente = actual.siguiente.siguiente; 
			actual.siguiente.anterior = actual; tama�o--;
			if(actualL.elemento == eliminado){
				if(tama�o == 0) actualL = null;
				else next();
			}
		}
		return eliminado;
	}

	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new MiIterator<T>(primera);
	}

	//FIXME
	private static class Node<T>
	{
		T elemento;
		Node<T> anterior;
		Node<T> siguiente;

		public Node(T pElemento, Node<T> pSiguiente, Node<T> pAnterior)
		{
			elemento = pElemento;
			siguiente = pSiguiente;
			anterior = pAnterior;
		}
	}
	
	public class MiIterator<T> implements Iterator<T>
	{
		Node<T> actual;
		public MiIterator(Node<T> primera){
			actual = primera;
		}

		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actual != null;
		}

		public T next() {
			// TODO Auto-generated method stub
			T elemento = actual.elemento;
			actual = actual.siguiente;
			return elemento;
		}
		
	}
}
